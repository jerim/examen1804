const mongoose = require('mongoose')
const Schema = mongoose.Schema

const materiaSchema = new Schema({
  codigo: {
    type: String,
    required: true
  },
  nombre: {
    type: String,
    required: true
  },
  curso: {
    type: String
  },
  horas: {
    type: Date,
    default: Date.now
  }
})

const Materia = mongoose.model('Materia', materiaSchema)

module.exports = Materia
