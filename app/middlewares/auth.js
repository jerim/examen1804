const servicejwt = require('../services/servicejwt.js')

const auth = (req, res, next) => {
  console.log(req.headers.authorization)
  if (!req.headers.authorization) {
    return res.status(403).send({ message: 'No tienes permiso' })
  }
  const token = req.headers.authorization.split(' ')[1] // aqui tiene el token
  // Authorization: Bearer XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  try {
    payload = servicejwt.decodeToken(token)
  } catch (error) {
    return res.status(401).send(`${error}`)
  }
  next()
  // res.status(200).send({ message: 'con permiso' })
}

module.exports = { auth }
