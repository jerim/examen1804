const Materia = require('../models/Materia.js')

const index = (req, res) => {
  Materia.find((err, materias) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo de la materia'
      })
    }
    return res.json(materias)
  })
}

const create = (req, res) => {
  const Materias = new Materia()
  Materias.codigo = req.body.codigo
  Materias.nombre = req.body.nombre
  Materias.curso = req.body.curso

  Materias.save((err, materias) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar la materia',
        error: err
      })
    }
    return res.status(201).json(materias)
  })
}

const { ObjectId } = require('mongodb') // 12 bytes
const show = (req, res) => {
  const id = req.params.id
  Materia.findOne({ _id: id }, (err, materias) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).json({ mensaje: 'id no valido' })
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al obtener la materia'
      })
    }
    if (!materias) {
      return res.status(404).json({
        message: 'No tenemos esta materia'
      })
    }
    return res.json(materias)
  })
}

const update = (req, res) => {
  const id = req.params.id // parametros de la ruta entre :
  Materia.findById({ _id: id }, (err, materias) => {
    // busqueda por id
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar la materia',
        error: err
      })
    }

    if (!materias) {
      return res.status(404).json({
        message: 'No hemos encontrado la materia'
      })
    }

    Object.assign(materias, req.body)

    materias.save((err, materias) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar la materia'
        })
      }
      if (!materias) {
        return res.status(404).json({
          message: 'No hemos encontrado la materia'
        })
      }
      return res.json(materias)
    })
  })
}
const destroy = (req, res) => {
  const id = req.params.id
  Materia.findByIdAndDelete(id, (err, data) => {
    if (err) return res.status(500).json({ message: 'error' })
    return res.json(data)
  })
}

module.exports = {
  index,
  create,
  show,
  update,
  destroy
}
