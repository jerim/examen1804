const express = require('express') // crear enrutador
const router = express.Router()
const userController = require('../controllers/userController') // llama al controlador

// registrar a un nuevo usuario
router.post('/', userController.register) // con parenteris no enlaza

module.exports = router
