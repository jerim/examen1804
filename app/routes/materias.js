const express = require('express')
const router = express.Router()
const materiaController = require('../controllers/materiaController.js')
const servicejwt = require('../services/servicejwt.js')
const moment = require('moment')
const auth = require('../middlewares/auth.js')

router.use(auth.auth)

router.get('/', (req, res) => {
  console.log(' en el ruta de materias')
  materiaController.index(req, res)
})

router.post('/', (req, res) => {
  console.log(' en el ruta create')
  materiaController.create(req, res)
})

router.put('/:id', (req, res) => {
  console.log(' en el ruta update')
  materiaController.update(req, res)
})

router.get('/:id', (req, res) => {
  console.log(' en el ruta show')
  materiaController.show(req, res)
})

router.delete('/:id', (req, res) => {
  console.log(' en el ruta destroy')
  materiaController.destroy(req, res)
})
module.exports = router
