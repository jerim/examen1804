const express = require('express')
const router = express.Router()

const routerMaterias = require('./routes/materias.js')
const routerUsers = require('./routes/users.js')

router.use('/materias', routerMaterias)
router.use('/users', routerUsers)
module.exports = router
